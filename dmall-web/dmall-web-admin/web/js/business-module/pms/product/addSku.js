layui.use(['form', 'crud'], function () {
    var crud = layui.crud;
    var form = layui.form;
    var $ = layui.$;

    // 规格列表
    var specificationsArr = [];

    // 查询商品详情获取规格列表
    crud.get(pmsUrl + '/product/' + id, function (response) {
        var data = response.data;
        var ext = data.ext;
        specificationsArr = ext.specifications;
        initSpecification();
    }, true);

    function initSpecification() {
        var specificationHtml = '';
        for (var i = 0; i < specificationsArr.length; i++) {
            specificationHtml += '<div  class="layui-form-item">';
            var specification = specificationsArr[i];
            specificationHtml += '<label class="layui-form-label">选择' + specification.attributeName + '</label>';
            specificationHtml += '<div class="layui-input-inline">';
            if (specification.attributeValues) {
                for (var j = 0; j < specification.attributeValues.length; j++) {
                    var attributeValue = specification.attributeValues[j];
                    specificationHtml += '<input type="radio" name="' + specification.attributeName + '"  value="' + attributeValue.attributeValue + '" lay-skin="primary"  title="' + attributeValue.attributeValue + '" >';
                }
            }
            specificationHtml += '</div></div>';
        }
        $("#specification").html('').append(specificationHtml);
        form.render();
    }

});
